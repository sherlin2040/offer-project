<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Affiliate extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model('affiliate_model');
        if(!$this->session->userdata('USRID'))
            $this->session->set_userdata('USRID', 'UTO-00002');
	}

	function generateRandomString($length = 10) 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) 
	    {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    $rand_fromdb = $this->affiliate_model->query('SELECT userID FROM usersignup WHERE userReferral="'.$randomString.'"');
	    if($rand_fromdb != NULL)
	    {
	    	generateRandomString();
	    }
	    else
	    {
		    return $randomString;
	    }
	}


	public function index()
	{
		// $menu['main_nav'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID=0 AND categoryIsActive=1 AND categoryShownInMenu=1 ORDER BY categorySortOrder');
		// $menu['section1'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=1');
		// $menu['section2'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=2');
		// $menu['section3'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=3');
		// $menu['section4'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=4');
		// $menu['section5'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=5');
		// $menu['categories'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1');
		// $user['user'] = $this->affiliate_model->query('SELECT userFirstName, userLastName FROM usersignup WHERE userID="'.$this->session->userdata('USRID').'"');
		// $user['wishlistcount'] = $this->affiliate_model->query('SELECT COUNT(productID) AS wishlistcount FROM wishlist WHERE userID="'.$this->session->userdata('USRID').'"');
		// $user['main_nav'] = $menu['main_nav'];

		// $data['products'] = $this->affiliate_model->query('SELECT * FROM products LIMIT 10');
		// $data['main_banners'] = $this->affiliate_model->query('SELECT * FROM homemainbanner ORDER BY id DESC');
		// $data['side_banners'] = $this->affiliate_model->query('SELECT * FROM homesidebanner ORDER BY id DESC');

		$this->load->view('inc/head');
		$this->load->view('inc/header');
		$this->load->view('pages/home/banner');
		// $this->load->view('inc/head_title', $user);
		// $this->load->view('inc/menu', $menu);
		// $this->load->view('pages/home/affiliate', $data);
		// $this->load->view('inc/footer_banner');
		$this->load->view('inc/footer');
	}

	public function product($shortid="")
	{
		error_reporting(0);
		$clicks = $this->affiliate_model->selectData('productclicks', 'clickProductGenID', $shortid, 'clickUserID', $this->session->userdata('USRID'), '', '');
		if($clicks == NULL)
		{
			$data_click = array(
								'clickProductGenID' => $shortid,
								'clickUserID' => $this->session->userdata('USRID'),
								'clickCount' => 1
								);
			$this->affiliate_model->insertData('productclicks', $data_click);
		}
		else
		{
			$click_count = 0;
			foreach($clicks as $click)
			{
				$click_count += $click->clickCount;
			}
			$data_click = array(
								'clickCount' => ++$click_count
								);
			$this->affiliate_model->update_data('productclicks', $data_click, 'clickProductGenID', 'clickUserID', $shortid, $this->session->userdata('USRID'));
		}

		$products = $this->affiliate_model->selectData('products', 'productGenerateID', $shortid, '', '', '', '');
			$product_id = $products[0]->productID;
		$data['affiliates'] = $this->affiliate_model->selectData('productaffiliatedetails', 'productID', $product_id, '', '', '', '');
		$data['images'] = $this->affiliate_model->selectData('productimages', 'productID', $product_id, '', '', '', '');
		$data['specs'] = $this->affiliate_model->selectData('productspecs', 'productID', $product_id, '', '', '', '');
		$data['mainspecs'] = $this->affiliate_model->query('SELECT productSpecHeading FROM productspecs WHERE productID="'.$product_id.'" GROUP BY productSpecHeading ORDER BY productSpecID');
		$data['product'] = $products;
		$menu['main_nav'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID=0 AND categoryIsActive=1 AND categoryShownInMenu=1 ORDER BY categorySortOrder');
		$menu['section1'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=1');
		$menu['section2'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=2');
		$menu['section3'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=3');
		$menu['section4'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=4');
		$menu['section5'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=5');
		$menu['categories'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1');
		$user['user'] = $this->affiliate_model->query('SELECT userFirstName, userLastName FROM usersignup WHERE userID="'.$this->session->userdata('USRID').'"');
		$user['wishlistcount'] = $this->affiliate_model->query('SELECT COUNT(productID) AS wishlistcount FROM wishlist WHERE userID="'.$this->session->userdata('USRID').'"');
		$user['main_nav'] = $menu['main_nav'];


		$this->load->view('inc/header');
		$this->load->view('inc/head_title', $user);
		$this->load->view('inc/menu', $menu);
		$this->load->view('pages/product/product', $data);
		$this->load->view('inc/footer_banner');
		$this->load->view('inc/footer');
	}


	public function wishlist()
	{
		$data['mobiles'] = $this->affiliate_model->selectData('products1', 'product_category', 'Mobiles', '', '', '', '');
		error_reporting(0);
		$menu['main_nav'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID=0 AND categoryIsActive=1 AND categoryShownInMenu=1 ORDER BY categorySortOrder');
		$menu['section1'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=1');
		$menu['section2'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=2');
		$menu['section3'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=3');
		$menu['section4'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=4');
		$menu['section5'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1 AND categorySectionNum=5');
		$menu['categories'] = $this->affiliate_model->query('SELECT * FROM categories WHERE categoryParentID!=0 AND categoryIsActive=1 AND categoryShownInMenu=1');
		$user['user'] = $this->affiliate_model->query('SELECT userFirstName, userLastName FROM usersignup WHERE userID="'.$this->session->userdata('USRID').'"');
		$user['main_nav'] = $menu['main_nav'];


		$this->load->library('flipkart');
		$this->load->library('amazon');
		$products = $this->affiliate_model->selectData('products1', 'product_shortlink', $shortlink, '', '', '', '');
		foreach($products as $product)
			$product_id = $product->id;
		$prod_affid = $this->affiliate_model->selectData('affiliate_products', 'product_id', $product_id, '', '', '', '');
		$prices = array();
		$site_details = array();
		foreach($prod_affid as $prodaffid)
		{
			$product_affid = $prodaffid->affiliate_productid;
			switch($prodaffid->site_name)
			{
				case 'flipkart':
					$prices += array('flipkart' => $this->flipkart->productPrice($product_affid));
					$site_details += array('flipkart' => $this->flipkart->productFind($product_affid));//Flipkart price
					break;
				case 'amazon': 
					$prices += array('amazon' => $this->amazon->AmazonproductPrice($product_affid));
					$site_details += array('amazon' => $this->amazon->AmazonproductFind($product_affid));//snapdeal price
					break;
				case 'snapdeal': 
					$prices += array('snapdeal' => 27999);
					$site_details += array('snapdeal' => 'test');//snapdeal price
					break;
			}
		}
		$minIndex = array_search(min($prices), $prices);
		$data['minProdPrice'] = array('site_name' => $minIndex, 'minPrice' => min($prices));
		$data['prices_list'] = $prices;
		$data['product_data'] = $this->affiliate_model->selectData('products1', 'id', $product_id, '', '', '', '');
		$data['sites'] = $this->affiliate_model->selectData('affiliation_sites', '', '', '', '', '', '');
		$data['site_details'] = $site_details;

		$this->load->view('inc/header');
		$this->load->view('inc/head_title', $user);
		$this->load->view('inc/menu', $menu);
		$this->load->view('pages/wishlist/wishlist', $data);
		$this->load->view('inc/footer_banner');
		$this->load->view('inc/footer');
	}

	public function test()
	{
		echo $this->generateRandomString();
	}

}
