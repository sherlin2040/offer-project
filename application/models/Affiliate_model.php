<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Affiliate_model extends CI_Model {
   
    public function userid($table)
    {
        $this->db->select_max('userID');
        $query = $this->db->get($table);
        $row = $query->row_array();
        
        if(isset($row['userID']))
        {
            $u_id = $row['userID'];
            list($user, $id) = explode("-", $u_id);
            $id += 1;
            $id = str_pad($id, 5, '0', STR_PAD_LEFT); 
            $user_id = "UTO-".$id;
        }
        else
        {
            $user_id = "UTO-00001";
        }
        return $user_id;
    }

    public function is_login()
    {
        if($this->session->userdata('USRID'))
            return true;
        return false;
    }

    public function login_check()
    {
        $this->db->where('userName', $this->input->post('email'));
        
        $query = $this->db->get('userlogin');
        
        if($query->num_rows() == 1)//if username and password found and match
        {
            $row = $query->row_array();
            if($this->bcrypt->check_password($this->input->post('password'), $row['userPassword']))
            {
                $userID = $row['userID'];
                $this->session->set_userdata('USRID', $userID);//set user id in session
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function login_check_username()
    {
        $this->db->where('userName', $this->input->post('email'));
        $query = $this->db->get('userlogin');
        if($query->num_rows() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function login_check_email($email)
    {
        $this->db->where('userEmailID', $email);
        $query = $this->db->get('usersignup');
        if($query->num_rows() == 1)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function login_check_mobile($mobile)
    {
        $this->db->where('userMobileNo', $mobile);
        $query = $this->db->get('usersignup');
        if($query->num_rows() == 1)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function selectData($table, $where1, $value1, $where2, $value2, $where3, $value3)
    {
        if($where1 != "" && $value1 != "")
        {
            $this->db->where($where1, $value1);
        }
        if($where2 != "" && $value2 != "")
        {
            $this->db->where($where2, $value2);
        }
        if($where3 != "" && $value3 != "")
        {
            $this->db->where($where3, $value3);
        }
        $this->db->from($table);
        $query = $this->db->get();
        $row = $query->result();
        return $row;
    }

    public function selectDataOrderby($table, $where1, $value1, $where2, $value2, $orderby, $order)
    {
        $this->db->order_by($orderby, $order);
        if($where1 != "" && $value1 != "")
        {
            $this->db->where($where1, $value1);
        }
        if($where2 != "" && $value2 != "")
        {
            $this->db->where($where2, $value2);
        }
        $this->db->from($table);
        $query = $this->db->get();
        $row = $query->result();
        return $row;
    }
    
    public function query($sql)
    {
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_product_minprice($productid) 
    {
        $this->db->select_min('productAffiliateSP'); 
        $this->db->from('productaffiliatedetails');
        $this->db->where('productID', $productid);
        $query = $this->db->get ();
        return $query->result ();
    } 

    function get_product_stockfilter($price,$productid) 
    {
        $this->db->select('productAffiliateInstock'); 
        $this->db->from('productaffiliatedetails');
        $this->db->where('productAffiliateSP', $price);
        $this->db->where('productID', $productid);
        $query = $this->db->get ();
        return $query->result ();
    } 

    function get_product_thumb($productid) 
    {
        $this->db->select('productThumbImages'); 
        $this->db->from('productimages');
        $this->db->where('productID', $productid);
        $this->db->limit(1);
        $query = $this->db->get ();
        return $query->result ();
    } 

    function get_product_brand($id) 
    {
        $this->db->select('brandName'); 
        $this->db->from('brands');
        $this->db->where('id', $id);
        $query = $this->db->get ();
        return $query->result ();
    } 

    function get_product_minurl($price,$productid) 
    {
        $this->db->select('productAffiliateDetails'); 
        $this->db->from('productaffiliatedetails');
        $this->db->where('productID', $productid);
        $this->db->where('productAffiliateSP', $price);
        $query = $this->db->get ();
        return $query->result ();
    } 

    function check_sub($categoryid) 
    {
        $this->db->select('*'); 
        $this->db->from('categories');
        $this->db->where('categoryParentID', $categoryid);
        $this->db->where('categoryShownInMenu', 1);
        $this->db->where('categoryIsActive', 1);
        $query = $this->db->get ();
        return $query->result ();
    } 

    public function insertData($table, $data)
    {
        $this->db->insert($table, $data);
    }

    public function update_data($table, $data, $rowname1, $rowname2, $value1, $value2)
    {
        if($rowname1 != NULL && $value1 != NULL)
        {
            $this->db->where($rowname1, $value1);
        }
        if($rowname2 != NULL && $value2 != NULL)
        {
            $this->db->where($rowname2, $value2);
        }
        $this->db->update($table, $data);
    }
}