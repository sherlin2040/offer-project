<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flipkart {

	public function productFind($pid)
	{
		//$searchstring = 'wd-my-passport-ultra-1-tb-wired-external-hard-disk-drive'; // Your search query
		//$url = 'https://affiliate-api.flipkart.net/affiliate/search/json?query='.$searchstring.'&resultCount=1';

		//$pid = "MOBEJFHUFVAJ45YA "; // Provide our product ID here
		$urlNew = 'https://affiliate-api.flipkart.net/affiliate/product/json?id=' .$pid;

	    //The headers are required for authentication
	    $headers = array(
	            'Cache-Control: no-cache',
	            'Fk-Affiliate-Id: '.'sherlin20',// Your flipcart affilicate ID
	            'Fk-Affiliate-Token: '.'f9cc08bfdc7d46eb81bfe6037559b335'// your affiliate token
	    );

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $urlNew);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    //curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-ClusterDev-Flipkart/0.1');
	    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    $result = curl_exec($ch);
	    curl_close($ch);        
	    $json = json_decode($result);       
	  //  $allResults = $json->productInfoList;
	    if(isset($json->productBaseInfo->productAttributes))
	    {
	    	return($json->productBaseInfo->productAttributes);        
	    }
	    else
	    {
	    	return NULL;
	    }
	}

	public function productPrice($pid="")
	{
		$allResults = $this->productFind($pid);
		if(isset($allResults->sellingPrice->amount))
		{
			return($allResults->sellingPrice->amount);
		}
		else
		{
			return "0";
		}
	}
}