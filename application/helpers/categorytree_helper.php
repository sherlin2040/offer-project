<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//convert tree array to tree view

function create_tree_view($elements){
    $elements = json_decode(json_encode($elements),TRUE);

    return buildTreeView(buildTreeArray($elements));
}

function create_tree_delete_view($elements){
    $elements = json_decode(json_encode($elements),TRUE);

    return buildTreeDeleteView(buildTreeArray($elements));
}

function create_tree_array_view($elements){
    $elements = json_decode(json_encode($elements),TRUE);

    return (buildTreeArray($elements));
}


function buildTreeArray(array $elements, $parentId = 0) {    
    $branch = array();

    foreach ($elements as $element) {
        if ($element['categoryParentID'] == $parentId) {
            $children = buildTreeArray($elements, $element['categoryID']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }
    return $branch;
}


function buildTreeView($arrays){
    $output = '<ul style="display:none;">';
    foreach ($arrays as $key => $array) {
        if (isset($array['children']) && is_array($array['children'])) {
            $output .= '<li class="folder">' . $array['categoryName'].''. buildTreeView($array['children']) . '</li>';
        } else {
            $output .= '<li>' . $array['categoryName'] . '</li>';
        }
    }
    $output .= '</ul>';
    return $output;
}

function buildTreeDeleteView($arrays){
    $output = '<ul class="delete">';
    foreach ($arrays as $key => $array) {
        if (isset($array['children']) && is_array($array['children'])) {
            $output .= '<li class="delete"><a href="'.site_url('admin-categories-edit/'.$array['categoryID']).'">'.$array['categoryName'].'</a>'.buildTreeDeleteView($array['children']).'</li>';
        } else {
            $output .= '<li><a href="'.site_url('admin-categories-edit/'.$array['categoryID']).'">'.$array['categoryName'].'</a></li>';
        }
    }
    $output .= '</ul>';
    return $output;
}