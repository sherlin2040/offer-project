<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function sorting_filter_array($query)
{
	$specs = explode("--",$query);
	$allspecs = array();
	foreach($specs as $spec)
	{
		list($spectitle,$specdetail) = explode("~",$spec);
		array_push($allspecs, array('title' =>$spectitle, 'detail' => $specdetail));
	}
	//print_r($allspecs);

	$tmp = array();

	foreach($allspecs as $arg)
	{
	    $tmp[$arg['title']][] = $arg['detail'];
	}

	$output = array();

	foreach($tmp as $type => $labels)
	{
	    $output[] = array(
	        'title' => $type,
	        'detail' => $labels
	    );
	}

	return $output;
}