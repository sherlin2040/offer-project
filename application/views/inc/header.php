<!-- ==========================HEADER======================== -->
<header>
	<div class="background-header"></div>
	<div class="slider-header">
		<!-- Top Bar -->
		<div id="top-bar" class="full-width">
			<div class="background-top-bar"></div>
			<div class="background">
				<div class="pattern">
					<div class="container">
						<div class="row">
							<!-- Top Bar Left -->
							<div class="col-sm-6">
							</div>
							<!-- Top Bar Right -->
							<div class="col-sm-6 text-right">
								<!-- Links -->
								<ul class="header-links">
									<li><a href="<?php echo site_url('wishlist'); ?>" id="wishlist-total">Wish List (0)</a></li>
									<li><a href="<?php echo site_url('account'); ?>">My Account</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Top of pages -->
		<div id="top" class="full-width">
			<div class="background-top"></div>
			<div class="background">
				<div class="pattern">
					<div class="container">
						<div class="overflow-header">
							<!-- Header Left -->
							<div id="header-left">
								<!-- Logo -->
								<div class="logo">
									<a href="<?php echo base_url(); ?>"><img src="img/logo-sello.png" title="Your Store" alt="Your Store" /></a>
								</div>
							</div>

							<!-- Header Right -->
							<div id="header-right">
								<!-- Search -->
								<div class="search_form">
									<div class="button-search"></div>
									<input type="text" class="input-block-level search-query" name="search" placeholder="Search..." id="search_query" value="" />
									<div id="autocomplete-results" class="autocomplete-results"></div>
								</div>

							</div>
						</div>
					</div>

				<div class="megamenu-background">
				<div class="">
				<div class="overflow-megamenu container"><div class="background-megamenu">
				<div class="row mega-menu-modules"><div class="col-md-3">
				<div id="megamenu_33504870" class="container-megamenu container vertical">
				<div id="menuHeading">
				<div class="megamenuToogle-wrapper">
				<div class="megamenuToogle-pattern">
				<div class="container">
				Categories				</div>
				</div>
				</div>
				</div>
				<div class="megamenu-wrapper">
				<div class="megamenu-pattern">
				<div class="container">
				<ul class="megamenu shift-left">
				<li class='' ><p class='close-menu'></p><p class='open-menu'></p><a href='index98dc.html?route=product/category&amp;path=20' class='clearfix' ><span><strong>Notebooks</strong></span></a></li>
				<li class=' with-sub-menu hover' ><p class='close-menu'></p><p class='open-menu'></p><a href='index98dc.html?route=product/category&amp;path=20' class='clearfix' ><span><strong>Phones</strong></span></a><div class="sub-menu" style="width:338%"><div class="content" ><p class="arrow"></p><div class="row"><div class="col-sm-12  mobile-enabled"><div class="row"><div class="col-sm-4 static-menu"><div class="menu"><ul><li><a href="index68ea.html?route=product/category&amp;path=33" onclick="window.location = 'index68ea.html?route=product/category&amp;path=33';" class="main-menu with-submenu">Cameras</a><div class="open-categories"></div><div class="close-categories"></div><ul><li><a href="index1647.html?route=product/category&amp;path=25" onclick="window.location = 'index1647.html?route=product/category&amp;path=25';">Components</a></li><li><a href="indexc219.html?route=product/category&amp;path=25_29" onclick="window.location = 'indexc219.html?route=product/category&amp;path=25_29';">Mice and Trackballs</a></li><li><a href="indexe177.html?route=product/category&amp;path=25_28" onclick="window.location = 'indexe177.html?route=product/category&amp;path=25_28';">Monitors</a></li><li><a href="indexf3db.html?route=product/category&amp;path=25_31" onclick="window.location = 'indexf3db.html?route=product/category&amp;path=25_31';">Scanners</a></li></ul></li><li><a href="indexe177.html?route=product/category&amp;path=25_28" onclick="window.location = 'indexe177.html?route=product/category&amp;path=25_28';" class="main-menu with-submenu">Monitors</a><div class="open-categories"></div><div class="close-categories"></div><ul><li><a href="indexc219.html?route=product/category&amp;path=25_29" onclick="window.location = 'indexc219.html?route=product/category&amp;path=25_29';">Mice and Trackballs</a></li><li><a href="indexf345.html?route=product/category&amp;path=20_27" onclick="window.location = 'indexf345.html?route=product/category&amp;path=20_27';">Mac</a></li><li><a href="index1647.html?route=product/category&amp;path=25" onclick="window.location = 'index1647.html?route=product/category&amp;path=25';">Components</a></li><li><a href="index8122.html?route=product/category&amp;path=34" onclick="window.location = 'index8122.html?route=product/category&amp;path=34';">MP3 Players</a></li></ul></li></ul></div></div><div class="col-sm-4 static-menu"><div class="menu"><ul><li><a href="index68a7.html?route=product/category&amp;path=25_30" onclick="window.location = 'index68a7.html?route=product/category&amp;path=25_30';" class="main-menu with-submenu">Printers</a><div class="open-categories"></div><div class="close-categories"></div><ul><li><a href="indexd9fe.php?route=product/category&amp;path=20_26" onclick="window.location = 'indexd9fe.php?route=product/category&amp;path=20_26';">PC</a></li><li><a href="index0b4e.html?route=product/category&amp;path=25_28_35" onclick="window.location = 'index0b4e.html?route=product/category&amp;path=25_28_35';">test 1</a></li><li><a href="index1647.html?route=product/category&amp;path=25" onclick="window.location = 'index1647.html?route=product/category&amp;path=25';">Components</a></li><li><a href="indexc957.html?route=product/category&amp;path=24" onclick="window.location = 'indexc957.html?route=product/category&amp;path=24';">Phones &amp; PDAs</a></li></ul></li><li><a href="indexf3db.html?route=product/category&amp;path=25_31" onclick="window.location = 'indexf3db.html?route=product/category&amp;path=25_31';" class="main-menu with-submenu">Scanners</a><div class="open-categories"></div><div class="close-categories"></div><ul><li><a href="indexb152.html?route=product/category&amp;path=17" onclick="window.location = 'indexb152.html?route=product/category&amp;path=17';">Software</a></li><li><a href="index68ea.html?route=product/category&amp;path=33" onclick="window.location = 'index68ea.html?route=product/category&amp;path=33';">Cameras</a></li><li><a href="index1647.html?route=product/category&amp;path=25" onclick="window.location = 'index1647.html?route=product/category&amp;path=25';">Components</a></li><li><a href="indexe177.html?route=product/category&amp;path=25_28" onclick="window.location = 'indexe177.html?route=product/category&amp;path=25_28';">Monitors</a></li></ul></li></ul></div></div><div class="col-sm-4 static-menu"><div class="menu"><ul><li><a href="indexe177.html?route=product/category&amp;path=25_28" onclick="window.location = 'indexe177.html?route=product/category&amp;path=25_28';" class="main-menu with-submenu">Monitors</a><div class="open-categories"></div><div class="close-categories"></div><ul><li><a href="indexf345.html?route=product/category&amp;path=20_27" onclick="window.location = 'indexf345.html?route=product/category&amp;path=20_27';">Mac</a></li><li><a href="indexe177.html?route=product/category&amp;path=25_28" onclick="window.location = 'indexe177.html?route=product/category&amp;path=25_28';">Monitors</a></li><li><a href="index8122.html?route=product/category&amp;path=34" onclick="window.location = 'index8122.html?route=product/category&amp;path=34';">MP3 Players</a></li><li><a href="index1647.html?route=product/category&amp;path=25" onclick="window.location = 'index1647.html?route=product/category&amp;path=25';">Components</a></li></ul></li><li><a href="index68ea.html?route=product/category&amp;path=33" onclick="window.location = 'index68ea.html?route=product/category&amp;path=33';" class="main-menu with-submenu">Cameras</a><div class="open-categories"></div><div class="close-categories"></div><ul><li><a href="index1647.html?route=product/category&amp;path=25" onclick="window.location = 'index1647.html?route=product/category&amp;path=25';">Components</a></li><li><a href="indexf3db.html?route=product/category&amp;path=25_31" onclick="window.location = 'indexf3db.html?route=product/category&amp;path=25_31';">Scanners</a></li><li><a href="indexb152.html?route=product/category&amp;path=17" onclick="window.location = 'indexb152.html?route=product/category&amp;path=17';">Software</a></li><li><a href="indexf345.html?route=product/category&amp;path=20_27" onclick="window.location = 'indexf345.html?route=product/category&amp;path=20_27';">Mac</a></li></ul></li></ul></div></div></div></div></div></div></div></li>
				<li class='' ><p class='close-menu'></p><p class='open-menu'></p><a href='index98dc.html?route=product/category&amp;path=20' class='clearfix' ><span><strong>Tablets</strong></span></a></li>
				<li class='' ><p class='close-menu'></p><p class='open-menu'></p><a href='index98dc.html?route=product/category&amp;path=20' class='clearfix' ><span><strong>Speakers</strong></span></a></li>
				<li class='' ><p class='close-menu'></p><p class='open-menu'></p><a href='index98dc.html?route=product/category&amp;path=20' class='clearfix' ><span><strong>Accessories</strong></span></a></li>
				<li class=' with-sub-menu hover' ><p class='close-menu'></p><p class='open-menu'></p><a href='index98dc.html?route=product/category&amp;path=20' class='clearfix' ><span><strong>Home cinemas</strong></span></a><div class="sub-menu" style="width:338%"><div class="content" ><p class="arrow"></p><div class="row"><div class="col-sm-8  mobile-enabled"><div class="row"><div class="col-sm-6 static-menu"><div class="menu"><ul><li><a href="#" onclick="window.location = '#';" class="main-menu with-submenu">Accessories</a><div class="open-categories"></div><div class="close-categories"></div><ul><li><a href="index68ea.html?route=product/category&amp;path=33" onclick="window.location = 'index68ea.html?route=product/category&amp;path=33';">Cameras</a></li><li><a href="index1647.html?route=product/category&amp;path=25" onclick="window.location = 'index1647.html?route=product/category&amp;path=25';">Components</a></li><li><a href="index8122.html?route=product/category&amp;path=34" onclick="window.location = 'index8122.html?route=product/category&amp;path=34';">MP3 Players</a></li><li><a href="indexe177.html?route=product/category&amp;path=25_28" onclick="window.location = 'indexe177.html?route=product/category&amp;path=25_28';">Monitors</a></li><li><a href="index1647.html?route=product/category&amp;path=25" onclick="window.location = 'index1647.html?route=product/category&amp;path=25';">Components</a></li><li><a href="index68ea.html?route=product/category&amp;path=33" onclick="window.location = 'index68ea.html?route=product/category&amp;path=33';">Cameras</a></li><li><a href="index8122.html?route=product/category&amp;path=34" onclick="window.location = 'index8122.html?route=product/category&amp;path=34';">MP3 Players</a></li><li><a href="index9f41.html?route=product/category&amp;path=18_46" onclick="window.location = 'index9f41.html?route=product/category&amp;path=18_46';">Macs</a></li><li><a href="indexe177.html?route=product/category&amp;path=25_28" onclick="window.location = 'indexe177.html?route=product/category&amp;path=25_28';">Monitors</a></li></ul></li></ul></div></div><div class="col-sm-6 static-menu"><div class="menu"><ul><li><a href="#" onclick="window.location = '#';" class="main-menu with-submenu">Collections</a><div class="open-categories"></div><div class="close-categories"></div><ul><li><a href="indexf3db.html?route=product/category&amp;path=25_31" onclick="window.location = 'indexf3db.html?route=product/category&amp;path=25_31';">Scanners</a></li><li><a href="index68ea.html?route=product/category&amp;path=33" onclick="window.location = 'index68ea.html?route=product/category&amp;path=33';">Cameras</a></li><li><a href="indexf345.html?route=product/category&amp;path=20_27" onclick="window.location = 'indexf345.html?route=product/category&amp;path=20_27';">Mac</a></li><li><a href="index1647.html?route=product/category&amp;path=25" onclick="window.location = 'index1647.html?route=product/category&amp;path=25';">Components</a></li><li><a href="indexf345.html?route=product/category&amp;path=20_27" onclick="window.location = 'indexf345.html?route=product/category&amp;path=20_27';">Mac</a></li><li><a href="indexe177.html?route=product/category&amp;path=25_28" onclick="window.location = 'indexe177.html?route=product/category&amp;path=25_28';">Monitors</a></li><li><a href="indexf3db.html?route=product/category&amp;path=25_31" onclick="window.location = 'indexf3db.html?route=product/category&amp;path=25_31';">Scanners</a></li><li><a href="indexb152.html?route=product/category&amp;path=17" onclick="window.location = 'indexb152.html?route=product/category&amp;path=17';">Software</a></li><li><a href="index68ea.html?route=product/category&amp;path=33" onclick="window.location = 'index68ea.html?route=product/category&amp;path=33';">Cameras</a></li></ul></li></ul></div></div></div></div><div class="col-sm-4  mobile-enabled"><a href="#"><img src="image/catalog/buy-sello.png" alt="" style="display: block;margin: 0px auto"></a></div></div></div></div></li>
				<li class='' ><p class='close-menu'></p><p class='open-menu'></p><a href='index98dc.html?route=product/category&amp;path=20' class='clearfix' ><span><strong>Smart TV</strong></span></a></li>
				<li class='' ><p class='close-menu'></p><p class='open-menu'></p><a href='index98dc.html?route=product/category&amp;path=20' class='clearfix' ><span><strong>Cameras</strong></span></a></li>
				<li class='' ><p class='close-menu'></p><p class='open-menu'></p><a href='index98dc.html?route=product/category&amp;path=20' class='clearfix' ><span><strong>Others</strong></span></a></li>
				</ul>
				</div>
				</div>
				</div>
				</div>

				<script type="text/javascript">
				$(window).load(function(){
				var css_tpl = '<style type="text/css">';
				css_tpl += '#megamenu_33504870 ul.megamenu > li > .sub-menu > .content {';
				css_tpl += '-webkit-transition: all 200ms ease-out !important;';
				css_tpl += '-moz-transition: all 200ms ease-out !important;';
				css_tpl += '-o-transition: all 200ms ease-out !important;';
				css_tpl += '-ms-transition: all 200ms ease-out !important;';
				css_tpl += 'transition: all 200ms ease-out !important;';
				css_tpl += '}</style>'
				$("head").append(css_tpl);
				});
				</script>
				</div><div class="col-md-9">
				<div id="megamenu_28319457" class="container-megamenu container horizontal mobile-disabled">
				<div class="megaMenuToggle">
				<div class="megamenuToogle-wrapper">
				<div class="megamenuToogle-pattern">
				<div class="container">
				<div><span></span><span></span><span></span></div>
				Navigation				</div>
				</div>
				</div>
				</div>
				<div class="megamenu-wrapper">
				<div class="megamenu-pattern">
				<div class="container">
				<ul class="megamenu shift-up">
				<li class=' with-sub-menu hover' ><p class='close-menu'></p><p class='open-menu'></p><a href='index98dc.html?route=product/category&amp;path=20' class='clearfix' ><span><strong>Categories</strong></span></a><div class="sub-menu" style="width:200px"><div class="content" ><p class="arrow"></p><div class="row"><div class="col-sm-12  mobile-enabled"><div class="row"><div class="col-sm-12 hover-menu"><div class="menu"><ul><li><a href="index68ea.html?route=product/category&amp;path=33" onclick="window.location = 'index68ea.html?route=product/category&amp;path=33';" class="main-menu ">Cameras</a></li><li><a href="index1647.html?route=product/category&amp;path=25" onclick="window.location = 'index1647.html?route=product/category&amp;path=25';" class="main-menu ">Components</a></li><li><a href="indexe177.html?route=product/category&amp;path=25_28" onclick="window.location = 'indexe177.html?route=product/category&amp;path=25_28';" class="main-menu ">Monitors</a></li><li><a href="indexf3db.html?route=product/category&amp;path=25_31" onclick="window.location = 'indexf3db.html?route=product/category&amp;path=25_31';" class="main-menu ">Scanners</a></li><li><a href="indexb152.html?route=product/category&amp;path=17" onclick="window.location = 'indexb152.html?route=product/category&amp;path=17';" class="main-menu ">Software</a></li><li><a href="indexe177.html?route=product/category&amp;path=25_28" onclick="window.location = 'indexe177.html?route=product/category&amp;path=25_28';" class="main-menu with-submenu">Monitors</a><div class="open-categories"></div><div class="close-categories"></div><ul><li><a href="index8122.html?route=product/category&amp;path=34" onclick="window.location = 'index8122.html?route=product/category&amp;path=34';" class="">MP3 Players</a></li></ul></li><li><a href="index9f41.html?route=product/category&amp;path=18_46" onclick="window.location = 'index9f41.html?route=product/category&amp;path=18_46';" class="main-menu ">Macs</a></li></ul></div></div></div></div></div></div></div></li>
				<li class='' ><p class='close-menu'></p><p class='open-menu'></p><a href='index8816.html?route=information/information&amp;information_id=4' class='clearfix' ><span><strong>About us</strong></span></a></li>
				<li class='' ><p class='close-menu'></p><p class='open-menu'></p><a href='index2724.html?route=information/contact' class='clearfix' ><span><strong>Contact</strong></span></a></li>
				<li class='' ><p class='close-menu'></p><p class='open-menu'></p><a href='index56b1.html?route=module/faq' class='clearfix' ><span><strong>Faq</strong></span></a></li>
				<li class='' ><p class='close-menu'></p><p class='open-menu'></p><a href='index8024.html?route=blog/blog' class='clearfix' ><span><strong>Blog</strong></span></a></li>
				</ul>
				</div>
				</div>
				</div>
				</div>

				<script type="text/javascript">
				$(window).load(function(){
				var css_tpl = '<style type="text/css">';
				css_tpl += '#megamenu_28319457 ul.megamenu > li > .sub-menu > .content {';
				css_tpl += '-webkit-transition: all 200ms ease-out !important;';
				css_tpl += '-moz-transition: all 200ms ease-out !important;';
				css_tpl += '-o-transition: all 200ms ease-out !important;';
				css_tpl += '-ms-transition: all 200ms ease-out !important;';
				css_tpl += 'transition: all 200ms ease-out !important;';
				css_tpl += '}</style>'
				$("head").append(css_tpl);
				});
				</script>
				</div></div>									</div>
				</div>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</header>
