<!-- FOOTER
================================================== -->
     <div class="footer full-width">
          <div class="background">
               <div class="pattern">
                    <div class="container">
                         <div class="row">
                              <!-- Information -->
                              <div class="col-sm-3">
                                   <h4>Information</h4>
                                   <div class="strip-line"></div>
                                   <ul>
                                        <li><a href="index8816.html?route=information/information&amp;information_id=4">About Us</a></li>
                                        <li><a href="index1766.html?route=information/information&amp;information_id=6">Delivery Information</a></li>
                                        <li><a href="index1679.html?route=information/information&amp;information_id=3">Privacy Policy</a></li>
                                        <li><a href="index99e4.html?route=information/information&amp;information_id=5">Terms &amp; Conditions</a></li>
                                        </ul>
                                   </div>

                              <!-- Customer Service -->
                              <div class="col-sm-3">
                                   <h4>Customer Service</h4>
                                   <div class="strip-line"></div>
                                   <ul>
                                        <li><a href="index2724.html?route=information/contact">Contact Us</a></li>
                                        <li><a href="index71ba.html?route=account/return/add">Returns</a></li>
                                        <li><a href="index7cb2.html?route=information/sitemap">Site Map</a></li>
                                   </ul> 
                              </div>

                              <!-- Extras -->
                              <div class="col-sm-3">
                                   <h4>Extras</h4>
                                   <div class="strip-line"></div>
                                   <ul>
                                        <li><a href="indexd773.html?route=product/manufacturer">Brands</a></li>
                                        <li><a href="index4dd2.html?route=account/voucher">Gift Certificates</a></li>
                                        <li><a href="index3d18.html?route=affiliate/account">Affiliates</a></li>
                                        <li><a href="indexf110.html?route=product/special">Specials </a></li>
                                   </ul>
                              </div>

                              <!-- My Account -->
                              <div class="col-sm-3">
                                   <h4>My Account</h4>
                                   <div class="strip-line"></div>
                                   <ul>
                                        <li><a href="index839c.php?route=account/account">My Account</a></li>
                                        <li><a href="indexe223.html?route=account/order">Order History</a></li>
                                        <li><a href="indexe223.html?route=account/wishlist">Wish List</a></li>
                                        <li><a href="indexe223.html?route=account/newsletter">Newsletter</a></li>
                                   </ul>
                              </div>

                         </div>
                    </div>
               </div>
          </div>
     </div>

     <!-- COPYRIGHT
     ================================================== -->
     <div class="copyright full-width">
          <div class="background">
               <div class="pattern">
                         <div class="line"></div>
                         <ul>
                              <li><img src="image/catalog/paypal.png" alt=""></li>
                              <li><img src="image/catalog/moneybookers.png" alt=""></li>
                              <li><img src="image/catalog/mastercard.png" alt=""></li>
                              <li><img src="image/catalog/visa.png" alt=""></li>
                         </ul>
                         <p>COMBROSE &copy; 2017</p>
               </div>
          </div>
     </div>
     <script type="text/javascript" src="<?php echo base_url(); ?>js/megamenu.js"></script>
</div>
     <a href="#" class="scrollup"><i class="fa fa-chevron-up"></i></a>
</div>
</body>
</html>