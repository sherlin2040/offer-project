<!DOCTYPE html>
<!--[if IE 7]> <html lang="en" class="ie7 responsive" > <![endif]-->  
<!--[if IE 8]> <html lang="en" class="ie8 responsive" > <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9 responsive" > <![endif]-->  
<!--[if !IE]><!--> <html lang="en" class="responsive" > <!--<![endif]-->  

<!-- Mirrored from demo2.ninethemes.net/sello/opencart/1/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Feb 2017 05:39:41 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
	<title>Your Store</title>
	<base  />

	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<meta name="description" content="My Store" />
					
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Overpass:100,100i,200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/animate.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/stylesheet.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/responsive.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/menu.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/owl.carousel.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/filter_product.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/wide-grid.css" />

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/jquery/magnific/magnific-popup.css" media="screen" />

	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>libraries/mf/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/twitter-bootstrap-hover-dropdown.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/echo.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/common.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/tweetfeed.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap-notify.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-1.10.4.custom.min.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>libraries/jquery/magnific/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript">
		var responsive_design = 'yes';
	</script>
</head>	
<body class="common-home header1 always_show_vertical_menu">
	<div class="standard-body">
		<div id="main" class="">
