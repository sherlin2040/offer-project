<!-- MAIN CONTENT
	================================================== -->
<div class="main-content full-width home">
<div class="background-content"></div>
<div class="background">
<div class="shadow"></div>
<div class="pattern">
<div class="container">


	<div class="row">				
		<div class="col-md-3" id="column_left">
			<div style="height: 460px" class="hidden-xs hidden-sm"></div>
		</div>
		<div class="col-md-9">
			<div class="fullwidth" id="camera_1">
				<div class="camera_slider">
					<div class="spinner"></div>
					<div class="camera_wrap" id="camera_wrap_1" style="height: 430px">
						<div>
							<a href="#"><img src="<?php echo base_url('uploads/home-main-slider/slider-01.png') ?>" alt="Slider"></a>
						</div>
						<div>
							<a href="#"><img src="<?php echo base_url('uploads/home-main-slider/slider-01.png') ?>" alt="Slider"></a>
						</div>
					</div>
				</div>
			</div>
			<script type="text/javascript">
			 var camera_slider = $("#camera_wrap_1");
			    
			 camera_slider.owlCarousel({
			 	 slideSpeed : 300,
			  	 lazyLoad : true,
			     singleItem: true,
			     autoPlay: 7000,
			     stopOnHover: true,
			     navigation: true,
			     navigationText: false,
			       });

			$(window).load(function() {	
			  $("#camera_1 .spinner").fadeOut(200);
			  $("#camera_wrap_1").css("height", "auto");
			});	
			</script>						
			<div class="row">
				<div class="col-md-12"></div>
			</div>
		</div>
	</div>
